import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filho',
  templateUrl: './app-filho.component.html',
  styleUrls: ['./app-filho.component.css']
})
export class AppFilhoComponent implements OnInit {
  title = 'Componente filho';
  fromFilho = null;

  @Input() fromPai;
  @Output() quandoFilhoEscrever = new EventEmitter();

  writeToPai(message) {
    this.quandoFilhoEscrever.emit(message);
  }

  ngOnInit() {}
}
