import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Componente pai';
  fromPai = null;
  fromFilho = null;

  receiveMessageFromFilho(mensagemFilho) {
    this.fromFilho = mensagemFilho;
  }
}
